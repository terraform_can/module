data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
#data "aws_kms_key" "this" {
#  key_id = "alias/${var.key_alias}"
#}

data "aws_iam_policy_document" "s3_policies" {

  statement {
    sid    = "Enable Account Users to see"
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:GetObjectAttributes",
      "s3:GetObjectRetention",
      "s3:GetObjectTagging",
      "s3:GetObjectVersion"
    ]
    resources = ["arn:aws:s3:::${var.bucket_name}/*"]
    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", data.aws_caller_identity.current.account_id)
      ]
    }
    condition {
      test     = "StringLike"
      variable = "aws:PrincipalArn"
      values   = [format("arn:aws:iam::%s:root", data.aws_caller_identity.current.account_id)]
    }
  }
}