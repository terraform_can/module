variable "bucket_name" {
  description = "the name of the S3 Bucket"
  type        = string
}
variable "object_lock_retention_days" {
  description = "Set object lock retention as days."
  type        = number
  default     = 30
}
variable "deep_archive_days" {
  description = "if an objecgt is not access after 180 days it will be moved to deep archive for cost efficiency."
  type        = number
  default     = 180
}
variable "archive_days" {
  description = "if an objecgt is not access after 125 days it will be moved to archive for cost efficiency."
  type        = number
  default     = 125
}